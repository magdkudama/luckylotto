<?php

namespace RaffleBundle\Filters;

use Symfony\Component\HttpFoundation\Request;

class ItemFilters {
    private $type;

    private $country;

    private $city;

    private $min;

    private $max;

    public function __construct($type, $country, $city, $min, $max) {
        $this->type = $type;
        $this->country = $country;
        $this->city = $city;
        $this->min = $min;
        $this->max = $max;
    }

    public function getType() {
        return $this->type;
    }

    public function hasType() {
        return $this->type !== null;
    }

    public function getCountry() {
        return $this->country;
    }

    public function hasCountry() {
        return $this->country !== null;
    }

    public function getCity() {
        return $this->city;
    }

    public function hasCity() {
        return $this->city !== null;
    }

    public function getMin() {
        return $this->min;
    }

    public function hasMin() {
        return $this->min !== null;
    }

    public function getMax() {
        return $this->max;
    }

    public function hasMax() {
        return $this->max !== null;
    }

    public static function createFromRequest(Request $request) {
        return new ItemFilters(
            $request->query->get('type', null),
            $request->query->getInt('country', null),
            $request->query->getInt('city', null),
            $request->query->getInt('min', null),
            $request->query->getInt('max', null)
        );
    }
}
