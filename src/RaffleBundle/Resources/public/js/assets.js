function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(function() {
    var carouselSelector = $('.guided-filters .carousel');
    var selectedInfo = [];

    carouselSelector.carousel({
        wrap: false,
        keyboard: false
    }).carousel('pause');

    $('.home .guided-filters .carousel a.thumbnail').click(function() {
        selectedInfo.push($(this).data('id'));
        carouselSelector.carousel('next');
    });

    $('.home .guided-filters .carousel .country-selector').change(function() {
        var route = '/country/' + $(this).val() + '/cities';

        $.get(route, function(cities) {
            for (var index in cities) {
                var city = cities[index];
                $('.home .guided-filters .carousel .city-selector').append(
                    $('<option></option>').val(city.id).html(city.name)
                );
            }
        });
    });

    $('.home .guided-filters .carousel .city-selector').change(function() {
        selectedInfo.push($('.home .guided-filters .carousel .country-selector').val());
        selectedInfo.push($(this).val());

        var route = '/price-range/' + selectedInfo[0];

        $.get(route, function(range) {
            $('.home .guided-filters .carousel .price-range input').ionRangeSlider({
                type: "double",
                grid: true,
                min: range.min,
                max: range.max,
                from: range.min,
                to: range.max,
                prefix: range.sign
            });
        });

        carouselSelector.carousel('next');
    });

    $('.home .guided-filters .carousel .price-range button').click(function() {
        var range = $('.home .guided-filters .carousel .price-range input').val().split(';');
        var rangeString = '&min=' + range[0] + '&max=' + range[1];
        var qs = '?type=' + selectedInfo[0] + '&country=' + selectedInfo[1] + '&city=' + selectedInfo[2] + rangeString;
        location.href = '/search' + qs;
    });

    var countrySelector = $('.search #country');

    if ($('body').hasClass('search')) {
        if (countrySelector.val()) {
            var route = '/country/' + countrySelector.val() + '/cities';

            $.get(route, function(cities) {
                var citySelector = $('.search #city');
                citySelector.find('option').remove();
                citySelector.append($('<option></option>').val('').html('-- Any --'));
                for (var index in cities) {
                    var city = cities[index];
                    var option = $('<option></option>').val(city.id).html(city.name);
                    if (city.id == getParameterByName('city')) {
                        option.prop('selected', true);
                    }
                    citySelector.append(option);
                }
            });
        }
    }

    countrySelector.change(function() {
        var route = '/country/' + $(this).val() + '/cities';

        $.get(route, function(cities) {
            var citySelector = $('.search #city');
            citySelector.find('option').remove();
            citySelector.append($('<option></option>').val('').html('-- Any --'));
            for (var index in cities) {
                var city = cities[index];
                citySelector.append(
                    $('<option></option>').val(city.id).html(city.name)
                );
            }
        });
    });
});
