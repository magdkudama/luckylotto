<?php

namespace RaffleBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="item")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"house" = "RaffleBundle\Entity\Items\House"})
 * @ORM\Entity(repositoryClass="RaffleBundle\Entity\Repository\ItemRepository")
 */
abstract class Item {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer", name="ticket_price")
     */
    private $ticketPrice;

    /**
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

    /**
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="Vendor", inversedBy="sellingItems")
     * @ORM\JoinColumn(name="vendor_id", referencedColumnName="id")
     */
    private $vendor;

    /**
     * @ORM\ManyToOne(targetEntity="Owner", inversedBy="sellingItems")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(type="datetime", name="start_selling_date")
     */
    private $startSellingDate;

    /**
     * @ORM\Column(type="datetime", name="end_selling_date")
     */
    private $endSellingDate;

    /**
     * @ORM\ManyToMany(targetEntity="Customer", mappedBy="tickets")
     */
    private $customers;

    public function __construct() {
        $this->customers = new ArrayCollection;
    }

    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    /**
     * @return Currency
     */
    public function getCurrency() {
        return $this->currency;
    }

    public function setCurrency(Currency $currency) {
        $this->currency = $currency;
    }

    /**
     * @return City
     */
    public function getCity() {
        return $this->city;
    }

    public function setCity(City $city) {
        $this->city = $city;
    }

    public function isEnabled() {
        return $this->enabled;
    }

    public function setEnabled($enabled) {
        $this->enabled = $enabled;
    }

    public function getStartSellingDate() {
        return $this->startSellingDate;
    }

    public function setStartSellingDate(\DateTime $startSellingDate) {
        $this->startSellingDate = $startSellingDate;
    }

    public function getEndSellingDate() {
        return $this->endSellingDate;
    }

    public function setEndSellingDate(\DateTime $endSellingDate) {
        $this->endSellingDate = $endSellingDate;
    }

    public function getTicketPrice() {
        return $this->ticketPrice;
    }

    public function setTicketPrice($ticketPrice) {
        $this->ticketPrice = $ticketPrice;
    }

    /**
     * @return Vendor
     */
    public function getVendor() {
        return $this->vendor;
    }

    public function setVendor(Vendor $vendor) {
        $this->vendor = $vendor;
    }

    /**
     * @return Owner
     */
    public function getOwner() {
        return $this->owner;
    }

    public function setOwner(Owner $owner) {
        $this->owner = $owner;
    }

    /**
     * @return Customer[]
     */
    public function getCustomers() {
        return $this->customers;
    }

    public function setCustomers($customers) {
        $this->customers = $customers;
    }

    public function addCustomer(Customer $customer) {
        $this->customers->add($customer);
    }

    public function removeCustomer(Customer $customer) {
        $this->customers->removeElement($customer);
    }
}
