<?php

namespace RaffleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="RaffleBundle\Entity\Repository\CategoryConfigurationRepository")
 * @ORM\Table(name="category_configuration")
 */
class CategoryConfiguration {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $minPrice;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxPrice;

    /**
     * @ORM\Column(type="string")
     */
    private $image;

    public function getId() {
        return $this->id;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getMinPrice() {
        return $this->minPrice;
    }

    public function setMinPrice($minPrice) {
        $this->minPrice = $minPrice;
    }

    public function getMaxPrice() {
        return $this->maxPrice;
    }

    public function setMaxPrice($maxPrice) {
        $this->maxPrice = $maxPrice;
    }

    public function getImage() {
        return $this->image;
    }

    public function setImage($image) {
        $this->image = $image;
    }
}
