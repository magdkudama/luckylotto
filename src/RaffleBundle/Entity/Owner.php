<?php

namespace RaffleBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="owner")
 */
class Owner {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $identifier;

    /**
     * @ORM\Column(type="string")
     */
    protected $identifierType;

    /**
     * @ORM\Column(type="string")
     */
    protected $phone;

    /**
     * @ORM\OneToOne(targetEntity="Customer", inversedBy="owner")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    protected $customer;

    /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="owner")
     */
    protected $sellingItems;

    public function __construct() {
        $this->sellingItems = new ArrayCollection;
    }

    public function getId() {
        return $this->id;
    }

    /**
     * @return Item[]
     */
    public function getSellingItems() {
        return $this->sellingItems;
    }

    public function setSellingItems(ArrayCollection $sellingItems) {
        $this->sellingItems = $sellingItems;
    }

    public function addSellingItem(Item $item) {
        $this->sellingItems->add($item);
    }

    public function removeSellingItem(Item $item) {
        $this->sellingItems->removeElement($item);
    }

    public function getIdentifier() {
        return $this->identifier;
    }

    public function setIdentifier($identifier) {
        $this->identifier = $identifier;
    }

    public function getIdentifierType() {
        return $this->identifierType;
    }

    public function setIdentifierType($identifierType) {
        $this->identifierType = $identifierType;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    /**
     * @return Customer
     */
    public function getCustomer() {
        return $this->customer;
    }

    public function setCustomer(Customer $customer) {
        $this->customer = $customer;
    }
}
