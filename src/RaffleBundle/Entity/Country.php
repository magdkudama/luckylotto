<?php

namespace RaffleBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="RaffleBundle\Entity\Repository\CountryRepository")
 * @ORM\Table(name="country")
 */
class Country {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="City", mappedBy="country")
     */
    private $cities;

    public function __construct() {
        $this->cities = new ArrayCollection;
    }

    public function getId() {
        return $this->id;
    }

    public function getCode() {
        return $this->code;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return City[]|ArrayCollection
     */
    public function getCities() {
        return $this->cities;
    }

    public function setCities($cities) {
        $this->cities = $cities;
    }

    public function addCity(City $city) {
        $this->cities->add($city);
    }

    public function removeCity(City $city) {
        $this->cities->removeElement($city);
    }
}
