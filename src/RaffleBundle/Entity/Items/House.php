<?php

namespace RaffleBundle\Entity\Items;

use Doctrine\ORM\Mapping as ORM;
use RaffleBundle\Entity\Embedded\Position;
use RaffleBundle\Entity\Item;

/**
 * @ORM\Entity
 * @ORM\Table(name="item_house")
 */
class House extends Item {

    /**
     * @ORM\Embedded(class="RaffleBundle\Entity\Embedded\Position")
     */
    private $position;

    public function __construct() {
        $this->position = new Position();
    }

    /**
     * @return Position
     */
    public function getPosition() {
        return $this->position;
    }

    public function setPosition(Position $position) {
        $this->position = $position;
    }
}
