<?php

namespace RaffleBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="vendor")
 */
class Vendor {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Customer", inversedBy="vendor")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    protected $customer;

    /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="vendor")
     */
    protected $sellingItems;

    public function __construct() {
        $this->sellingItems = new ArrayCollection;
    }

    public function getId() {
        return $this->id;
    }

    /**
     * @return Item[]
     */
    public function getSellingItems() {
        return $this->sellingItems;
    }

    public function setSellingItems(ArrayCollection $sellingItems) {
        $this->sellingItems = $sellingItems;
    }

    public function addSellingItem(Item $item) {
        $this->sellingItems->add($item);
    }

    public function removeSellingItem(Item $item) {
        $this->sellingItems->removeElement($item);
    }

    /**
     * @return Customer
     */
    public function getCustomer() {
        return $this->customer;
    }

    public function setCustomer(Customer $customer) {
        $this->customer = $customer;
    }
}
