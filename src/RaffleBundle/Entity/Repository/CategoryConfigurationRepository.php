<?php

namespace RaffleBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use RaffleBundle\Entity\CategoryConfiguration;

class CategoryConfigurationRepository extends EntityRepository {
    /**
     * @param $type
     * @return CategoryConfiguration
     */
    public function findByType($type) {
        try {
            $query = $this
                ->createQueryBuilder('cc')
                ->andWhere('cc.type = :type')
                ->setParameter('type', $type)
                ->getQuery();

            return $query->getSingleResult();
        } catch (\Exception $e) {
            return null;
        }
    }
}
