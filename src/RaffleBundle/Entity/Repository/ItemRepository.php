<?php

namespace RaffleBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use RaffleBundle\Entity\Item;
use RaffleBundle\Filters\ItemFilters;

class ItemRepository extends EntityRepository {
    /**
     * @return  Item[]
     */
    public function findRecommendedItems() {
        $rsm = new ResultSetMapping;

        $query = $this->_em->createNativeQuery("SELECT i.*, COUNT(*) as count FROM customer_item ci LEFT JOIN item i ON i.id = ci.item_id GROUP BY ci.customer_id ORDER BY count", $rsm);

        return $query->getResult();
    }

    public function search(ItemFilters $filters) {
        $q = $this
            ->createQueryBuilder('it')
            ->select('it, ci')
            ->innerJoin('it.city', 'ci');

        if ($filters->hasMin()) {
            $q
                ->andWhere('it.price >= :minPrice')
                ->setParameter('minPrice', $filters->getMin());
        }

        if ($filters->hasMax()) {
            $q
                ->andWhere('it.price <= :maxPrice')
                ->setParameter('maxPrice', $filters->getMax());
        }

        if ($filters->hasCity()) {
            $q
                ->andWhere('ci.id = :cityId')
                ->setParameter('cityId', $filters->getCity());
        }

        return $q->getQuery()->getResult();
    }
}
