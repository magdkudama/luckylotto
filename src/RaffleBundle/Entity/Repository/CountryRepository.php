<?php

namespace RaffleBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use RaffleBundle\Entity\Country;

class CountryRepository extends EntityRepository {
    /**
     * @param $id
     * @return Country
     */
    public function findCities($id) {
        $query = $this
            ->createQueryBuilder('co')
            ->select('co, ci')
            ->innerJoin('co.cities', 'ci')
            ->andWhere('co.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $query->getSingleResult();
    }
}
