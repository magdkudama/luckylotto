INSERT INTO `category_configuration` (`id`, `type`, `name`, `min_price`, `max_price`, `image`) VALUES
  (1, 'house', 'House', 50000, 3000000, 'house.png'),
  (2, 'car', 'Car', 2000, 300000, 'car.png');
