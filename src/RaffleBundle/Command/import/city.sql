INSERT INTO `city` (`id`, `name`, `country_id`) VALUES
  (1, 'Madrid', 1),
  (2, 'Barcelona', 1),
  (3, 'Castellon', 1),
  (4, 'Mallorca', 1),
  (5, 'London', 2),
  (6, 'Liverpool', 2),
  (7, 'Leeds', 2),
  (8, 'Brighton', 2);
