<?php

namespace RaffleBundle\Command;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ImportCommand extends ContainerAwareCommand {
    protected function configure() {
        $this
            ->setName('raffle:import')
            ->setDescription('Initial import for the database');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Are you sure you want to clean the database and insert test records?', false);

        if (!$helper->ask($input, $output, $question)) {
            return;
        }

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var Connection $connection */
        $connection = $em->getConnection();

        $output->writeln('Deleting tables...');

        $connection->exec('SET foreign_key_checks = 0');
        $connection->exec('TRUNCATE TABLE city');
        $connection->exec('TRUNCATE TABLE country');
        $connection->exec('TRUNCATE TABLE category_configuration');
        $connection->exec('SET foreign_key_checks = 1');

        $output->writeln('Tables dropped; starting process...');

        $output->writeln('Importing country records...');
        $connection->exec(file_get_contents(__DIR__ . '/import/country.sql'));

        $output->writeln('Importing categories records...');
        $connection->exec(file_get_contents(__DIR__ . '/import/categories.sql'));

        $output->writeln('Importing city records...');
        $connection->exec(file_get_contents(__DIR__ . '/import/city.sql'));

        $output->writeln('Finished executing import');
    }
}