<?php

namespace RaffleBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

class CountryController extends Controller {
    public function citiesAction($id) {
        $country = $this->getCountryRepository()->findCities($id);

        if (!$country) {
            return new JsonResponse('', 404);
        }

        return new JsonResponse($country->getCities()->toArray());
    }
}
