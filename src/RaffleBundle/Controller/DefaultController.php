<?php

namespace RaffleBundle\Controller;

use RaffleBundle\Filters\ItemFilters;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {
    public function indexAction() {
        return $this->render('RaffleBundle:Default:index.html.twig', [
            'types' => $this->getCategoryRepository()->findAll(),
            'countries' => $this->getCountryRepository()->findAll(),
            'recommended_items' => $this->getItemRepository()->findRecommendedItems()
        ]);
    }

    public function priceRangeAction($type) {
        $categoryConfiguration = $this->getCategoryRepository()->findByType($type);

        if ($categoryConfiguration) {
            return new JsonResponse([
                'min' => $categoryConfiguration->getMinPrice(),
                'max' => $categoryConfiguration->getMaxPrice(),
                'sign' => '$'
            ]);
        }

        return new JsonResponse('', 404);
    }

    public function searchAction(Request $request) {
        $items = $this->getItemRepository()->search(
            ItemFilters::createFromRequest($request)
        );

        return $this->render('RaffleBundle:Default:search.html.twig', [
            'types' => $this->getCategoryRepository()->findAll(),
            'countries' => $this->getCountryRepository()->findAll(),
            'items' => $items
        ]);
    }
}
