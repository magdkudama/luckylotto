<?php

namespace RaffleBundle\Controller;

use RaffleBundle\Entity\Repository\CategoryConfigurationRepository;
use RaffleBundle\Entity\Repository\CountryRepository;
use RaffleBundle\Entity\Repository\ItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;

class Controller extends BaseController {
    /**
     * @return CountryRepository
     */
    protected function getCountryRepository() {
        return $this->getDoctrine()->getManager()->getRepository('RaffleBundle:Country');
    }

    /**
     * @return ItemRepository
     */
    protected function getItemRepository() {
        return $this->getDoctrine()->getManager()->getRepository('RaffleBundle:Item');
    }

    /**
     * @return CategoryConfigurationRepository
     */
    protected function getCategoryRepository() {
        return $this->getDoctrine()->getManager()->getRepository('RaffleBundle:CategoryConfiguration');
    }
}
