<?php

namespace RaffleBundle\Controller;

class ItemController extends Controller {
    public function indexAction($slug, $id) {
        return $this->render('RaffleBundle:Default:index.html.twig', [
            'types' => $this->getCategoryRepository()->findAll(),
            'countries' => $this->getCountryRepository()->findAll(),
            'recommended_items' => $this->getItemRepository()->findRecommendedItems()
        ]);
    }
}
