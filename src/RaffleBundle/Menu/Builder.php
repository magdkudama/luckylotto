<?php

namespace RaffleBundle\Menu;

use Knp\Menu\FactoryInterface;

class Builder {
    public function mainMenu(FactoryInterface $factory, array $options) {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');
        $menu->addChild('Homepage', ['route' => 'raffle_homepage']);
        $menu->addChild('About us', ['route' => 'raffle_about']);

        return $menu;
    }
}
