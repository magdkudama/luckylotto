<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName')
                    ->add('lastName')
                    ->add('country', EntityType::class, array(
                            'class' => 'RaffleBundle:Country',
                            'choice_label' => 'name',
                        ));
        $builder->remove('username');
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_customer_registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}