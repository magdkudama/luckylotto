<?php

namespace UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use RaffleBundle\Entity\Customer;
use UserBundle\Form\RegistrationType;


class RegistrationController extends BaseController
{
    public function registerAction(Request $request)
    {
        $user = $this->getUser();
        if (is_object($user) && $user instanceof UserInterface) {
            return $this->redirectToRoute('fos_user_profile_show');
        }

        return parent::registerAction($request);
    }
}